//
//  ClothsService.swift
//  cloths
//
//  Created by Dmytro Golub on 09/10/2020.
//  Copyright © 2020 Dmytro Golub. All rights reserved.
//

import Foundation
import Combine

struct Product: Codable, Identifiable, Hashable {
    let id: Int
    let name, category, price: String
    let stock: Int
    let oldPrice: String?
    init(id: Int, name: String, category: String, price: String, oldPrice: String, stock: Int) {
        self.id = id
        self.name = name
        self.category = category
        self.price = price
        self.oldPrice = oldPrice
        self.stock = stock
    }
}

struct CartItem: Codable, Identifiable, Hashable {
    let id: Int
    let productId: Int

    init(id: Int, productId: Int) {
        self.id = id
        self.productId = productId
    }
}

enum ClothsServiceError: Error {
    case malformedURL
    case parser
    case network
}

protocol ClothsService {
    func getProducts() -> AnyPublisher<[Product], ClothsServiceError>
    func getCardItems() -> AnyPublisher<[CartItem], ClothsServiceError>
    func addItemToCart(with itemId: String) -> AnyPublisher<Never, ClothsServiceError>
    func deleteItemFromCart(with itemId: String) -> AnyPublisher<Never, ClothsServiceError>
}


final class ClothsServiceImpl: ClothsService {
    private let session: URLSession = .shared
    private let baseURL: String
    private let apiKey: String

    enum ServicePath: String {
        case products = "/cloths/products"
        case cart = "/cloths/cart"
    }

    enum HTTPMethod: String {
        case get = "GET"
        case post = "POST"
        case delete = "DELETE"
    }

    init(baseURL: String = "https://2klqdzs603.execute-api.eu-west-2.amazonaws.com", apiKey: String = "ddb86d7543-69dc-4119-ae50-7901d6b6b545") {
        self.baseURL = baseURL
        self.apiKey = apiKey
    }

    func getProducts() -> AnyPublisher<[Product], ClothsServiceError> {
        guard let request = buildRequest(with: .products) else {
            return Fail(error: ClothsServiceError.malformedURL).eraseToAnyPublisher()
        }
        return session.dataTaskPublisher(for: request)
            .mapError { error in
                return ClothsServiceError.network
        }.tryMap() { element -> Data in
            guard let httpResponse = element.response as? HTTPURLResponse,
                httpResponse.statusCode == 200 else {
                    throw ClothsServiceError.network
            }
            return element.data
        }
        .decode(type: [Product].self, decoder: JSONDecoder())
        .mapError { error in
            return .parser
        }
        .eraseToAnyPublisher()
    }

    func getCardItems() -> AnyPublisher<[CartItem], ClothsServiceError> {
        guard let request = buildRequest(with: .cart) else {
            return Fail(error: ClothsServiceError.malformedURL).eraseToAnyPublisher()
        }
        return session.dataTaskPublisher(for: request)
            .mapError { error in
                return ClothsServiceError.network
        }.tryMap() { element -> Data in
            guard let httpResponse = element.response as? HTTPURLResponse,
                httpResponse.statusCode == 200 else {
                    throw ClothsServiceError.network
            }
            return element.data
        }
        .decode(type: [CartItem].self, decoder: JSONDecoder())
        .mapError { error in
            return .parser
        }
        .eraseToAnyPublisher()
    }

    func addItemToCart(with itemId: String) -> AnyPublisher<Never, ClothsServiceError> {
        guard let request = buildRequest(with: .cart, method: .post, parameters: ["productId" : itemId]) else {
            return Fail(error: ClothsServiceError.malformedURL).eraseToAnyPublisher()
        }
        return session.dataTaskPublisher(for: request)
            .mapError { error in
                return ClothsServiceError.network
        }.ignoreOutput()
        .eraseToAnyPublisher()
    }

    func deleteItemFromCart(with itemId: String) -> AnyPublisher<Never, ClothsServiceError> {
        guard let request = buildRequest(with: .cart, method: .delete, parameters: ["id" : itemId]) else {
            return Fail(error: ClothsServiceError.malformedURL).eraseToAnyPublisher()
        }
        return session.dataTaskPublisher(for: request)
            .mapError { error in
                return ClothsServiceError.network
        }.ignoreOutput()
        .eraseToAnyPublisher()
    }

    private func buildRequest(with servicePath: ServicePath, method: HTTPMethod = .get, parameters: [String: String]? = nil) -> URLRequest? {
        var strUrl = "\(baseURL)\(servicePath.rawValue)"
        if let parameters = parameters {
            strUrl += "?"
            parameters.forEach { key, value in
                strUrl += "\(key)=\(value)"
            }
        }
        guard let url = URL(string: strUrl) else {
            return nil
        }
        var request = URLRequest(url: url)
        request.httpMethod = method.rawValue
        request.setValue(apiKey, forHTTPHeaderField: "X-API-KEY")
        return request
    }
}

//
//  ProductView.swift
//  cloths
//
//  Created by Dmytro Golub on 03/10/2020.
//  Copyright © 2020 Dmytro Golub. All rights reserved.
//

import ASCollectionView
import SwiftUI

struct ProductsView: View {
    private let products: [Product]
    private let service: ClothsService

    init(with products: [Product], service: ClothsService) {
        self.products = products
        self.service = service
    }

    var body: some View
    {
        ASCollectionView(data: products, dataID: \.self) { product, _ in
            NavigationLink(destination: ProductView(viewModel: ProductViewModel(product: product, service: self.service))) {
                VStack {
                    Image("placeholder")
                    Text(product.name).font(.body)
                    Text(product.itemPrice)
                    product.oldPrice.map{ Text(product.formattedPrice(with: $0)).strikethrough() }
                    Text(product.stock > 0 ? "Available" : "Not Available").font(.footnote)
                }
            }.buttonStyle(PlainButtonStyle())
        }
        .layout {
            .grid(layoutMode: .adaptive(withMinItemSize: 100),
                  itemSpacing: 5,
                  lineSpacing: 5,
                  itemSize: .estimated(70))
        }.navigationBarTitle(products.first?.category ?? "")
    }
}




extension Product {
    private static var formatter: NumberFormatter {
        let formatter = NumberFormatter()
        formatter.locale = Locale.current
        formatter.numberStyle = .currency
        return formatter
    }

    func formattedPrice(with string: String) -> String {
        format(string: string)
    }

    func formattedPrice(with string: String?) -> String? {
        guard let string = string else {return nil}
        return format(string: string)
    }
    var itemPrice: String {
        return formattedPrice(with: price)
    }

    private func format(string: String) -> String {
        if let number = Float(string) as NSNumber?,
            let formattedAmount = Product.formatter.string(from: number) {
            return formattedAmount
        }
        return string
    }
}

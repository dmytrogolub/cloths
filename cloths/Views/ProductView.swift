//
//  ProductView.swift
//  cloths
//
//  Created by Dmytro Golub on 11/10/2020.
//  Copyright © 2020 Dmytro Golub. All rights reserved.
//

import Foundation
import SwiftUI
import Combine

struct ProductViewState {
    let productName: String
    let price: String
    let oldPrice: String?
    let availability: Bool
}

final class ProductViewModel: ObservableObject, Identifiable {
    private let service: ClothsService
    private let product: Product
    private var disposables = Set<AnyCancellable>()

    @Published var viewState: ProductViewState
    @Published var alertPresented: Bool = false

    init(product: Product, service: ClothsService) {
        self.product = product
        self.service = service
        viewState = ProductViewState(productName: product.name,
                                     price: product.itemPrice,
                                     oldPrice: product.formattedPrice(with: product.oldPrice),
                                     availability: product.stock > 0)
    }

    func buyProduct() {
        service.addItemToCart(with: String(self.product.id))
            .receive(on: DispatchQueue.main)
            .sink(receiveCompletion: { [weak self] value in
                guard let self = self else { return }
                switch value {
                case .failure:
                    self.alertPresented = true
                case .finished:
                    break
                }
                }, receiveValue: { _ in })
            .store(in: &disposables)
    }
}

struct ProductView: View {
    @ObservedObject var viewModel: ProductViewModel

    init(viewModel: ProductViewModel) {
        self.viewModel = viewModel
    }

    var body: some View {
        VStack {
            Image("placeholder")
            Text(viewModel.viewState.price)
            viewModel.viewState.oldPrice.map{ Text($0).strikethrough() }
            Text(viewModel.viewState.availability ? "Available" : "Not Available").font(.footnote)
            Button(action: {
                self.viewModel.buyProduct()
            }) {
                Text("Buy")
                }.font(.largeTitle)
                .alert(isPresented: $viewModel.alertPresented) { () -> Alert in
                    Alert(title: Text("Error"), message: Text("Operation failed"), dismissButton: .default(Text("OK")))
            }.opacity(viewModel.viewState.availability ? 1 : 0)
            Button(action: {
                ()
            }) {
                Text("Add to Wish List")
            }
        }.navigationBarTitle(viewModel.viewState.productName)
    }
}

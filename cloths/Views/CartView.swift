//
//  CartView.swift
//  cloths
//
//  Created by Dmytro Golub on 10/10/2020.
//  Copyright © 2020 Dmytro Golub. All rights reserved.
//

import Foundation
import SwiftUI
import Combine

struct CartView: View {
    @ObservedObject var viewModel: CartViewModel

    init(viewModel: CartViewModel) {
        self.viewModel = viewModel
    }
    var body: some View {
        NavigationView {
            return contentView()
        }.onAppear() {
            self.viewModel.fetchCardItems()
        }
    }

    private func contentView() -> AnyView {
        switch(viewModel.viewState) {
        case .loading:
            return AnyView(Text("Loading ..."))
        case .empty:
            return AnyView(Text("No items in wish list"))
        case .items(let products):
            return AnyView(List {
                Section(footer: Text("Total: \(viewModel.totalPrice)")) {
                    ForEach(products) { product in
                        Text("\(product.name) \(product.itemPrice)")
                    }.onDelete(perform: delete)
                }
            }.navigationBarTitle("Cart")
                .listStyle(GroupedListStyle()))
        }
    }

    private func delete(at offsets: IndexSet) {
        viewModel.deleteItem(with: offsets)
    }
}

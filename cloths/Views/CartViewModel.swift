//
//  CartViewModel.swift
//  cloths
//
//  Created by Dmytro Golub on 10/10/2020.
//  Copyright © 2020 Dmytro Golub. All rights reserved.
//

import Foundation
import Combine

enum CartViewState {
    case empty
    case loading
    case items([Product])
}

final class CartViewModel: ObservableObject, Identifiable {
    @Published var viewState: CartViewState
    @Published var totalPrice: String = ""

    private let clothsService: ClothsService
    private var disposables = Set<AnyCancellable>()
    private var cartCache = [CartItem]()

    init(clothsService: ClothsService) {
        self.clothsService = clothsService
        self.viewState = .loading
    }

    func fetchCardItems() {
        viewState = .loading
        _ = Publishers.CombineLatest(clothsService.getCardItems(), clothsService.getProducts())
            .receive(on: DispatchQueue.main)
            .sink(
                receiveCompletion: { [weak self] value in
                    guard let self = self else { return }
                    switch value {
                    case .failure:
                        self.viewState = .empty
                    case .finished:
                        break
                    }
                },
                receiveValue: { [weak self] combined in
                    guard let self = self else { return }
                    if combined.0.isEmpty {
                        self.viewState = .empty
                    } else {
                        let items = combined.0.map { cartItem in
                            combined.1.filter { $0.id == cartItem.productId }
                        }
                        .flatMap { $0 }
                        self.viewState = .items(items)
                        let total = items.map { ($0.price as NSString).floatValue }
                        .reduce(Float(0), +)
                        self.totalPrice = self.formatPrice(price: total)
                        self.cartCache = combined.0
                    }
            })
            .store(in: &disposables)
    }

    func deleteItem(with offsets: IndexSet) {
        guard let idx = offsets.first,
            case .items(let products) = viewState,
            let cartId = cartCache.filter({ $0.productId == products[idx].id }).first else {
                return
        }
        let itemId = String(cartId.id)
        viewState = .loading
        clothsService.deleteItemFromCart(with: itemId).receive(on: DispatchQueue.main)
            .sink(receiveCompletion: { [weak self] value in
                guard let self = self else { return }
                switch value {
                case .failure:
                    break
                case .finished:
                    self.fetchCardItems()
                }
                }, receiveValue: { _ in })
            .store(in: &disposables)
    }

    private func formatPrice(price: Float) -> String {
        let formatter = NumberFormatter()
        formatter.locale = Locale.current
        formatter.numberStyle = .currency
        if let number = price as NSNumber?,
            let formattedAmount = formatter.string(from: number) {
            return formattedAmount
        }
        return String(describing: price)
    }
}

//
//  ContentView.swift
//  cloths
//
//  Created by Dmytro Golub on 02/10/2020.
//  Copyright © 2020 Dmytro Golub. All rights reserved.
//

import SwiftUI
import Combine

final class MainViewViewModel: ObservableObject, Identifiable {
    @Published var dataSource: [Product] = [] {
        didSet {
            var dict = [String: [Product]]()
            dataSource.forEach {
                var values = dict[$0.category] ?? []
                values.append($0)
                dict[$0.category] = values
            }
            categories = dict
        }
    }
    @Published var categories: [String: [Product]] = [:]

    let clothsService: ClothsService
    private var disposables = Set<AnyCancellable>()

    init(clothsService: ClothsService) {
        self.clothsService = clothsService
    }

    func fetchProducts() {
        clothsService.getProducts().receive(on: DispatchQueue.main)
            .sink(
                receiveCompletion: { [weak self] value in
                    guard let self = self else { return }
                    switch value {
                    case .failure:
                        self.dataSource = []
                    case .finished:
                        break
                    }
                },
                receiveValue: { [weak self] products in
                    guard let self = self else { return }
                    self.dataSource = products
            })
            .store(in: &disposables)
    }
}

struct MainView: View {
    @ObservedObject var viewModel: MainViewViewModel

    init(viewModel: MainViewViewModel) {
        self.viewModel = viewModel
    }

    var body: some View {
        TabView {
            if viewModel.dataSource.isEmpty {
                Text("Loading ...").onAppear(perform: viewModel.fetchProducts)
            } else {
                CategoriesView(products: viewModel.categories, clothsService: viewModel.clothsService)
                    .tabItem {
                        Text("Products")
                }.tag(0)
                WishListView()
                    .tabItem {
                        Text("Wish List")
                }.tag(1)
                CartView(viewModel: CartViewModel(clothsService: viewModel.clothsService))
                    .tabItem {
                        Text("Cart")
                }.tag(2)
            }
        }
    }
}

struct CategoriesView: View {
    private let products: [String: [Product]]
    private let clothsService: ClothsService

    private var caterories: [String] {
        return Array(products.keys)
    }
    init(products: [String: [Product]], clothsService: ClothsService) {
        self.products = products
        self.clothsService = clothsService
    }

    var body: some View {
        NavigationView {
            if products.count > 0 {
                List(self.caterories, id: \.self) { category in
                    NavigationLink(destination: ProductsView(with: self.products[category] ?? [],
                                                             service: self.clothsService)) {
                                                                Text(category)
                    }
                }.navigationBarTitle("Categories")
            }
            else {
                Text("Loading ...")
            }
        }
    }
}

struct WishListView: View {
    var body: some View {
        Color.green
    }
}
